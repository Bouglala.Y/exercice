Q1 En entrant la commande man mkdir cela me permet d'avoir accès à la documentation d'ubuntu sur cette commande en particulier mkdir .

Q2 Pour ce faire  il faut rentrer la commande suivante mkdir -p rep-toto/rep-titi/rep-tata.

Q3 Pour ce faire il faut aller dans le répertoire qui nous intéresse par exemple rep toto a l'aide de la commande cd rep-toto dans lequel on crée un fichier à l'aide de la commande touch toxo.txt et on répète le même procédé pour chacun.

Q4  Pour cela il faut taper la commande suivante ln -s rep-toto 42.

Q5  Pour cela il faut taper la commande ls -al et ls -lah pour avoir l’unité de mesure supérieure 

total 136K
drwxr-xr-x 1 youssef youssef  512 Mar 31 22:52 .
drwxr-xr-x 1 root    root     512 Mar 31 21:51 ..
-rw-r--r-- 1 youssef youssef  220 Mar 31 21:51 .bash_logout
-rw-r--r-- 1 youssef youssef 3.7K Mar 31 21:51 .bashrc
drwxr-xr-x 1 youssef youssef  512 Mar 31 21:52 .landscape
-rw-r--r-- 1 youssef youssef    0 Mar 31 21:52 .motd_shown
drwxr-xr-x 1 youssef youssef  512 Mar 31 22:09 .oh-my-zsh
-rw-r--r-- 1 youssef youssef  807 Mar 31 21:51 .profile
-rw-r--r-- 1 youssef youssef   10 Mar 31 22:09 .shell.pre-oh-my-zsh
-rw-r--r-- 1 youssef youssef    0 Mar 31 21:53 .sudo_as_admin_successful
-rw-r--r-- 1 youssef youssef  48K Mar 31 22:09 .zcompdump
-rw-r--r-- 1 youssef youssef  50K Mar 31 22:09 .zcompdump-DESKTOP-33QQ2RU-5.8
-rw------- 1 youssef youssef  717 Mar 31 22:52 .zsh_history
-rw-r--r-- 1 youssef youssef 3.7K Mar 31 22:09 .zshrc
lrwxrwxrwx 1 youssef youssef    8 Mar 31 22:47 42 -> rep-toto
-rw-r--r-- 1 root    root     498 Mar 31 22:47 exo.txt
drwxr-xr-x 1 youssef youssef  512 Mar 31 22:42 rep-toto 

Q6 Pour cela il faut taper la commande:sudo apt install zsh

Q7 Pour cela il faut taperla commande:SSH THEME = "robbyrusselli"

Q8 Q8 Pour cela il faut utiliser la commande:sudo apt vdm

Q9  La commande utiliser sudo update-alternatives --config editor puis on  sélectionne le 3eme choix pour avoir vim par défaut

Q10 La commande utiliser :sudo apt install git

Q11 Creation Alias
alias ci='git add'                                                                                                                                                               alias co='git add'
alias br='git add'
alias st='git add'


Q12 La commande à utiliser  cp -r rep-toto 42-cpy

Q13  Pour cela il faut utliser la commande mv 42-cpy 42-cpy-rename

Q14 il faut utiliser la commande cp -r 42-cpy-rename 42-cpy-rename-v2   rm -rf 42-cpy-rename

Q15 La commande a utliser ls -al

 total 148
 drwxr-xr-x 1 youssef youssef   512 Apr  1 00:48 .
 drwxr-xr-x 1 root    root      512 Mar 31 21:51 ..
 -rw------- 1 youssef youssef   181 Mar 31 23:18 .bash_history
 -rw-r--r-- 1 youssef youssef   220 Mar 31 21:51 .bash_logout
 -rw-r--r-- 1 youssef youssef  3771 Mar 31 21:51 .bashrc
 drwxr-xr-x 1 youssef youssef   512 Mar 31 21:52 .landscape
 -rw-r--r-- 1 youssef youssef     0 Mar 31 21:52 .motd_shown
 drwxr-xr-x 1 youssef youssef   512 Mar 31 22:09 .oh-my-zsh
 -rw-r--r-- 1 youssef youssef   807 Mar 31 21:51 .profile
 -rw-r--r-- 1 youssef youssef    10 Mar 31 22:09 .shell.pre-oh-my-zsh
 -rw-r--r-- 1 youssef youssef     0 Mar 31 21:53 .sudo_as_admin_successful
 drwxr-xr-x 1 youssef youssef   512 Mar 31 23:18 .vim
 -rw------- 1 youssef youssef  1572 Mar 31 23:18 .viminfo
 -rw-r--r-- 1 youssef youssef 49006 Mar 31 22:09 .zcompdump
 -rw-r--r-- 1 youssef youssef 50332 Mar 31 22:09 .zcompdump-DESKTOP-33QQ2RU-5.8
 -rw------- 1 youssef youssef  2295 Apr  1 00:48 .zsh_history
 -rw-r--r-- 1 youssef youssef  3690 Mar 31 22:09 .zshrc
 lrwxrwxrwx 1 youssef youssef     8 Mar 31 22:47 42 -> rep-toto
 drwxr-xr-x 1 youssef youssef   512 Apr  1 00:46 42-cpy-rename-v2
 -rw-r--r-- 1 root    root        2 Mar 31 23:21 exercice.txt
 -rw-r--r-- 1 root    root     5760 Apr  1 00:37 exo.txt
 drwxr-xr-x 1 youssef youssef   512 Mar 31 22:42 rep-toto

 Q16 Pour cela il faut utiliser la commande sudo adduser bot42

 Q17 La commande utiliser sudo visudo puis je rajoute bot42 ALL=(ALL:ALL) ALL

 Q18 Pour cela je me sert de la commande mkdir root-42

 Q19  Il faut utiliser la commande sudo rm -rf root-42

 Q20 La commande a utiliser wc -m 42-letter-count.txt

 Q21 Pour cela il faut utiliser la commande sudo chmod 755 42-user et aussi utiliser  sudo chown bot42 42-user pour changer le propriétaire du fichier

 Q22 Creation du fichier
 touch my-file-42
 puis utilisation de ls -cut
 résultat: my-file-42  42-user  42-cpy-rename-v2  rep-toto  test  variable
 42-letter-count.txt  exo.txt  testun.sh  variable1.sh  exercice.txt  42 

 Q23 Pour cela je me sert de la commande  crontab -e ensuite /bin/echo "i love 42" >> /home/youssef/my-log-42.log

 Q24  La commande a utiliser tail -f my-log-42.lo
 
 Q25 Pour cela il faut utiliser la commande Creation du fichier avec touch toto-42 puis ajout de beaucoup de texte afin d’arriver a 42Ko

 Q26 La commande a utiliser chmod 613 toto-42 

 Q27
 
 Q28

 Q29 La commande a utiliser: wc -l /etc/passwd
 et le résultat est 32 /etc/passwd

 Q30

